﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Level : MonoBehaviour
{
    public GameObject bustedUI;


    public Text pressE;

    public bool levelFail = false;
    private int levelWidth;
    private int levelHeight;

    public Transform floorTile;
    public Transform wallTile;
    public Transform sudokuTile;

    //array for tile colors, which will be used in void LoadLevel
    private Color[] tileColors;

    //public colors which will be declared in inspector, will transfer pixel colors from Photoshop/level layout file, and fill in appropriote and respecful tiles
    public Color floorTileColor;
    public Color wallTileColor;
    public Color sudokuColor;

    // this is the PSD file/ file with colors which will be used to feed the level layout info into Unity
    public Texture2D levelTexture;

     

    void Start()
    {
        // on start, set the levelWidth and levelHeight variables to that of the level texture import (64 x 64 pixels), then execute LoadLevel function
        levelWidth = levelTexture.width;
        levelHeight = levelTexture.height;
        LoadLevel();
    }


    void LoadLevel()
    {
        // set new colors array based on lW * lH, then set those pixels' color to the levelTexture pixels
        tileColors = new Color[levelWidth * levelHeight];
        tileColors = levelTexture.GetPixels();


        // for loop to set the tiles based on both level height and then a second inbeded for loop for level width
        for (int y = 0; y < levelHeight; y++)
        {
            for (int x = 0; x < levelWidth; x++)
            {
                // check the array of tile colors against the color set public color (set inside inspector), if it is == then Instantiate floor tile
                if (tileColors[x + y * levelWidth] == floorTileColor)
                {
                    // Instantiate from current floorTile position to new Vector3 (at the current x, y which the for loop is currently at), Quaternion.identity (returns nothing i think?)
                    Instantiate(floorTile, new Vector3(x, y), Quaternion.identity);
                }
                // same as above but for the wall tiles instead
                if (tileColors[x + y * levelWidth] == wallTileColor)
                {
                    Instantiate(wallTile, new Vector3(x, y), Quaternion.identity);
                }
                if (tileColors[x + y * levelWidth] == sudokuColor)
                {
                    Instantiate(sudokuTile, new Vector3(x, y), Quaternion.identity);
                }
            }
        }
    }

        public void LevelFail()
        {
            if(levelFail == true)
            {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name); 
        }
            
        }
    public void SudokuLoad()
    {
        SceneManager.LoadScene(2);
    }
    
}
