﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : MonoBehaviour
{
    public GameObject levelmanager;
    private Level levelscript;



    // declaring speed variable 
    public float speed;

    // discovered that SerializeField can make a private variable still appear in the inspector
    [SerializeField]
    private float verticalMove;

    [SerializeField] private float horizontalMove;

    // the rb attached to the player
    private Rigidbody2D rb;


    private bool isMoving;


    // on game awake, set rb (rigidbody) variable to the Rigidbody2D (if it is attached to the game object (player) this is true so it will use the one on the player.
    private void Awake()
    {

        rb = this.GetComponent<Rigidbody2D>();
    }
    void Start()
    {

    }

    void Update()

    {
        // using Unity project settings input to set variables for moving both directions. In hindsight it may not be necasary to create a variable and I could have possibyl just used 
        // GetAxisRaw without attaching it to variable (maybe?)
        horizontalMove = Input.GetAxisRaw("Horizontal");
        verticalMove = Input.GetAxisRaw("Vertical");

        // Attempting to create set speed of player based on if the player is moving or not, mostly this was code curiosity/ trying to experiment (It doesn't really affect movement, I think if anything it brings the player to a more sudden stop.
        if (Input.GetAxisRaw("Horizontal") == -1 || Input.GetAxisRaw("Horizontal") == 1 || Input.GetAxisRaw("Vertical") == -1 || Input.GetAxisRaw("Vertical") == 1)
        {
            isMoving = true;
            speed = 6f;
        }

        else if (isMoving == false)
        {

            speed = 0;
        }
    }



    void FixedUpdate()
    {
        if (rb != null)
        {
            // There are two Input.GetRawAxis, one for horizontal and one for vertical, it will provide a value of -1 if left (A or left arrow) or 1 if right (D or right arrow)
            // or on the vertical axis, -1 equals down (S or down arrow) or 1 equals up (W or up arrow). else the both axis will be equal to zero.

            // if verticalMove (which is updated by Input.GetAxisRaw("Vertical") is greater than 0 it will move player up, else if it is less than 0, it will move the player down
            if (verticalMove > 0)
            {

                rb.transform.position += Vector3.up * speed * Time.deltaTime;
            }

            else if (verticalMove < 0)
            {
                rb.transform.position += Vector3.down * speed * Time.deltaTime;
            }

            // same as above but for right and left
            if (horizontalMove > 0)
            {
                rb.transform.position += Vector3.right * speed * Time.deltaTime;
            }
            else if (horizontalMove < 0)
            {
                rb.transform.position += Vector3.left * speed * Time.deltaTime;
            }
            // attempting to see if setting isMoving to false (which sets speed to 0) will make movement stop quicker (experimental)
            else
            {
                isMoving = false;
            }
            Debug.Log("hM: " + horizontalMove);
            Debug.Log("vM: " + verticalMove);
            Debug.Log("speed: " + speed);

        }

    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("guard"))
        {
            Destroy(rb);

            FindObjectOfType<Level>().Invoke("LevelFail", 4f);
            FindObjectOfType<Level>().levelFail = true;
            FindObjectOfType<Level>().bustedUI.SetActive(true);
        }

        if (other.gameObject.CompareTag("sudoku"))
        {
            
            
                FindObjectOfType<Level>().Invoke("SudokuLoad", 0.5f);
            
        }

    }
}


