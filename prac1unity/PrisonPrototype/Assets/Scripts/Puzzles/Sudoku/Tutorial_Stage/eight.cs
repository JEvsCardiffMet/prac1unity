﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eight : MonoBehaviour
{

    [SerializeField]
    private Transform num8place;

    private Vector2 initialPOS;

    private Vector2 mousePOS;

    public static bool locked;

    private float deltaX, deltaY;

    // Start is called before the first frame update
    void Start()
    {
        initialPOS = transform.position;
    }


    private void OnMouseDown()
    {
        if (!locked)
        {
            deltaX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x;

            deltaY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y - transform.position.y;
        }
    }

    private void OnMouseDrag()
    {
        if (!locked)
        {
            mousePOS = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector2(mousePOS.x - deltaX, mousePOS.y - deltaY);
        }
    }

    private void OnMouseUp()
    {
        if (Mathf.Abs(transform.position.x - num8place.position.x) <= 1f &&
            Mathf.Abs(transform.position.y - num8place.position.y) <= 1f)
        {
            transform.position = new Vector2(num8place.position.x, num8place.position.y);
            locked = true;


        }
        else
        {
            transform.position = new Vector2(initialPOS.x, initialPOS.y);
        }

    }
}
