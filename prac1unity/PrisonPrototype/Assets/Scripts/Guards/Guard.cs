﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guard : MonoBehaviour
{
    public Transform player;
    private Rigidbody2D rbguard;
    private Vector2 movementtoplayer;
    public float guardspeed = 5.0f;

    void Start()
    {
        // at script start, get rigidbody of game object
        rbguard = this.GetComponent<Rigidbody2D>();
    }

    
    void Update()
    {
        // create Vector3 direction which is equal to player position minus the GameObject (guards) position
        Vector3 direction = player.position - transform.position;
        direction.Normalize();
        movementtoplayer = direction;
    }
    private void FixedUpdate()
    {
        // invoke(?) void guardChasePlayer with the parameter movementtoplayer
        guardChasePlayer(movementtoplayer);
    }

    void guardChasePlayer(Vector2 direction)
    {
        // guard will chase player from rigidbody and then moving that from current position to the direction of player multiplied by guard speed and then Time.deltaTime.
        rbguard.MovePosition((Vector2)transform.position + (direction * guardspeed * Time.deltaTime));
    }
}
